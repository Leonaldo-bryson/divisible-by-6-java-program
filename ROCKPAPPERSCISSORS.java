/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rock.papper.scissors;

/**
 *
 * @author LEO
 */import java.util.Scanner;
public class ROCKPAPPERSCISSORS {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner input = new Scanner(System.in);
        System.out.println("THIS IS A GAME OF ROCK-PAPER-SCISSORS PLAYED BY TWO");
        System.out.println("THERE ARE THREE OPTIONS(R, P & S),THE TWO PLAYER ARE TO MAKE A SINGLE PICK ONLY");
        System.out.println("R: REPRESENTS (ROCK) AND IS GREATER THAN SCISSORS");
        System.out.println("  P: REPRESENTS (PAPER) WHICH IS GREATER THAN ROCK");
        System.out.println("     S: REPRESENTS (SCISSORS) WHICH IS GREATER THAN PAPER");
        System.out.println("ENTER A VALUE P,R & S  PLAYER 1 ");
        char value1 = input.nextLine().charAt(0);
        System.out.println("ENTER A VALUE P,R & S PLAYER 2 ");
        char value2 = input.nextLine().charAt(0);
        boolean rock_scissors = value1 == 'r' && value2 == 's';
        boolean paper_rock = value1 == 'p' && value2== 'r';
        boolean scissors_paper = value1 == 's'  && value2 == 'p';
        if (rock_scissors || paper_rock || scissors_paper){
            System.out.println("PLAYER 1 WINS!!!!!!!!!!!");
        }
        else{
            System.out.println("PLAYER 2 WINS!!!!!!");
        }
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package feverprogram;

/**
 *
 * @author LEO
 */import java.util.Scanner;
public class Feverprogram {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        char fever;
        char cough;
        Scanner input = new Scanner(System.in);
        System.out.println("DO YOU HAVE A FEVER? Y/N");
        fever= input.nextLine().charAt(0);
        System.out.println("DO YOU HAVE A COUGH? Y/N");
        cough= input.nextLine().charAt(0);
        if (fever=='y' && cough=='y'){
            System.out.println("Your recommendation is to see a doctor AND rest.");
        }
        else if (fever=='n' && cough=='y'){
            System.out.println("Your recommendation is to get some rest.");
        }
        else if (fever=='y' && cough=='n'){
            System.out.println("Your recommendation is to see a doctor and stay in a ventilated room.");
        }
        else{
            System.out.println("Your are healthy.");
            // TODO code application logic here
        }
    }
}

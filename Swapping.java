/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swapping;

/**
 *
 * @author LEO
 */import java.util.Scanner;
public class Swapping {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("type in a sentence");
        String value1 = input.nextLine();
        String value2 = input.nextLine();
        String newvalue = value1;
        value1 = value2;
        value2= newvalue;
        System.out.println(value1);
        System.out.println(value2);
    }
}

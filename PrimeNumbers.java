/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prime.numbers;

/**
 *
 * @author LEO
 */
import java.util.Scanner;
public class PrimeNumbers {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("ENTER A VALUE FOR I");
        int value = input.nextInt();
        boolean two_three = (value == 2 || value == 3); 
        boolean one = (value == 1);
        boolean no_remainder = (value%2 == 0 || value%3 == 0);
        if (two_three){
            System.out.println("IS A PRIME NUMBER");
        }      
        else if (one || no_remainder ){
            System.out.println("IS NOT A PRIME NUMBER");
        }
        else{
            System.out.println("IS A PRIME NUMBER");
        }
    }
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minutes.to.numberofyearsanddays;

/**
 *
 * @author LEO
 */import java.util.Scanner;
public class MinutesToNUMberofyearsanddays {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("ENTER THE TOTAL NUMBER OF MINUTES");
        int totalminutes = input.nextInt();
        /* 1440mins = 1day 
        * 525600mins = 1 year
        * 24hours = 1day
        * 8760 hours = 1year
        * you can test with 3456789mins which is 6years 210days
        */
        int hours = totalminutes/60;
        int years= hours/8760;
        int availableminute = years * 525600;
        int minutes = totalminutes-availableminute;
        int days = minutes/1440;
        System.out.println("TOTAL NUMBER OF  YEARS & DAYS  :  " + years +  "years" + " " + days + "days"); 
    }
}
